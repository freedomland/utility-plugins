# Server utility plugins
Some helpers for server.

Most of them doesn't make sense since 0.7, but Unstack is still usable.

### Tools

|                |                                                                                                                                                                                                        |                                             |
|----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------|
| chargenMessage | Show message right after new character is created.                                                                                                                                                     | [how to install](chargenMessage/INSTALL.md) |
| Unstack        | Backport of /fixme command to 0.6.2 from pre-0.7. Still this is works better, because istead of using fixme from console it also setpos Z and after second called fixme. Now you can't stuck in rocks. | [how to install](Unstack/INSTALL.md)        |

### License
Server utility plugins (c) Volk_Milit, GPL v3.0.
