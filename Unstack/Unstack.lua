--[[
Backport a pre-0.7 command to 0.6.2. Original author Devid Cernat
Original command fixme is not working as expected so I decide to change it to setpos
But this script also used fixme
]]--

myMod = require("myMod")

local fixmeInterval = 5
local fixmeStrenght = 1000
--local restrictInterors = {"Balmora", "Ald-ruhn", "Seyda Neen", "Pelagiad"}

Methods = {}

function CheckExt(pid)
	local str = tes3mp.GetCell(pid)
	
	if str:match("%d+,") ~= nil and str:match("%a+,") == nil then
		return true
	end
	
	return false
end

function Methods.Fixme(pid, cell)
	local currentTime = os.time()
	
	local str = tes3mp.GetCell(pid)
	--[[for i, t in pairs(restrictInterors) do
		if str:match(t .. ",") ~= nil then
			tes3mp.MessageBox(pid, -1, "Здесь нельзя использовать fixme.")
			return
		end
	end]]--

	if Players[pid].data.customVariables.lastFixMe == nil or
		currentTime >= Players[pid].data.customVariables.lastFixMe + fixmeInterval then
		
		if CheckExt(pid) == true then
			local Z = Players[pid].data.location.posZ
		
			myMod.RunConsoleCommandOnPlayer(pid, "setpos Z " .. fixmeStrenght)
		
			currentTime = os.time()
		
			local i = 0
			while os.time() < currentTime + 1 do
				i = i + 1
			end
			
			myMod.RunConsoleCommandOnPlayer(pid, "fixme")
		else
			myMod.RunConsoleCommandOnPlayer(pid, "coc \"" .. str .. "\"")
		end
		
		Players[pid].data.customVariables.lastFixMe = currentTime
		tes3mp.MessageBox(pid, -1, "Похоже что вы выбрались.")
	else
		local remainingSeconds = Players[pid].data.customVariables.lastFixMe + fixmeInterval - currentTime
		local message = "Вы не сможете использовать /fixme еще "

		if remainingSeconds > 1 then
			message = message .. remainingSeconds .. " секунд"
		else
			message = message .. "секунду"
		end

		message = message .. "\n"
		tes3mp.MessageBox(pid, -1, message)
	end
end

return Methods
