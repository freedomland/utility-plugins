myMod = require("myMod")

Methods = {}

local places = {"balmora", "seyda neen", "vivec", "gnisis", "dagon fel", "tel mora", "tel fyr", "ald-ruhn"}

function Methods.MapFix(pid)
	for i, t in pairs(places) do
		myMod.RunConsoleCommandOnPlayer(pid, "showmap \"" .. t .. "\"")
	end
end

return Methods
