require("color")

Methods = {}

local messageFile = os.getenv("MOD_DIR") .. "/Messages/msg.txt"
local lines = nil

-- http://lua-users.org/wiki/FileInputOutput

-- see if the file exists
function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- get all lines from a file, returns an empty 
-- list/table if the file does not exist
function lines_from(file)
  if not file_exists(file) then return {} end
  lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end

function Methods.message(pid)
	local message = ""
	lines = lines_from(messageFile)
	
	for k,v in pairs(lines) do
		message = message .. v
	end
	
	tes3mp.SendMessage(pid, "\n" .. message .. "\n\n", false)
end

function Methods.messageBox(pid)
	if Players[pid].data.customVariables.messageOnce == nil or Players[pid].data.customVariables.messageOnce == false then
		tes3mp.MessageBox(pid, -1, color.BrandedFL .. "Для того чтобы писать в чате нажмите Y (по умолчанию).\n\n/a сообщение - чтобы написать в общий чат.\n\nДля получения полного списка команд наберите /help в чате.")
		Players[pid].data.customVariables.messageOnce = true
	end
end

return Methods
